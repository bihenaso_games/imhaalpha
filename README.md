# Space Guerilla Simulator Pre-alpha

Open source Pre-alpha version of Space Guerilla Simulation. Test some mechanics etc.

**USAGE**

If *node.js* is installed, open terminal in project file and run below command.

`node server.js`

If *python* installed, open terminal in project file and run below command.

`python -m SimpleHTTPServer 8000`

After that, open your browser and open *index.html* as below.

`http://localhost:8000/static/index.html`

See video [Run Space Guerrilla Simulator Pre-alpha in GNU/Linux via your web browser](https://www.youtube.com/watch?v=rlsxEhw_Ib8).

For Windows visit [link](https://gist.github.com/jgravois/5e73b56fa7756fd00b89), after then follow the steps described in the video.


**CREDITS**

Thanks for game assets [kenney.nl](https://www.kenney.nl/) or [kenney](https://opengameart.org/users/kenney).
Thanks for main menu assets [Kadir Şenyiğit](https://opengameart.org/users/Kadsyi).

**LICENCE**

Space Guerilla Simulator Pre-alpha by BIHENASO Games

License (CC0)
http://creativecommons.org/publicdomain/zero/1.0/

You may use these contents(codes or assets) in personal and commercial projects.
Credit [BIHENASO itch.io Page](https://bihenaso-games.itch.io/) or
[BIHENASO Google Play Page](https://play.google.com/store/apps/developer?id=BIHENASO+Games) would be nice but is not mandatory.
