var w = window,
	d = document,
	e = d.documentElement,
	g = d.getElementsByTagName('body')[0],
	x = w.innerWidth  || e.clientWidth  || g.clientWidth,
	y = w.innerHeight || e.clientHeight || g.clientHeight;
	
document.addEventListener('keydown', onKeyDown);
document.addEventListener('keyup', onKeyUp);

var path = "../assets/images/";
var level = getAllUrlParams().level;

var soundStatus = JSON.parse(localStorage.getItem("soundStatus"));
var difficultyStatus = JSON.parse(localStorage.getItem("difficultyStatus"));
var maxScore = JSON.parse(localStorage.getItem("maxScore"));

var pShip, app, bg, energyCounter, counterStep, proton, playerShield, playerBonusShield, energyStep, container;

var score = 0,
    scorePrompt = "Destroyed: ";
    scoreText =  scorePrompt + score,
    scoreLabel = null;

var energyItem, shieldItem, factorItem;

var bgTexture, playerShipTexture, enemyShipTexture, 
    playerLaserTexture, enemyLaserTexture, playerShieldTexture, playerBonusShieldTexture, enemyRadarTexture;
    
var playerFireTexture0, playerFireTexture1, enemyFireTexture0, enemyFireTexture1;

var meteorTexture0, meteorTexture1, meteorTexture2, meteorTexture3, meteorTexture4,
    meteorTexture5, meteorTexture6, meteorTexture7;
    
var playerDamageTexture0, playerDamageTexture1, playerDamageTexture2,
    enemyDamageTexture0, enemyDamageTexture1, enemyDamageTexture2;

var playerDeathTexture0, playerDeathTexture1, playerDeathTexture2, playerDeathTexture3,
    playerDeathTexture4, playerDeathTexture5, enemyDeathTexture0, enemyDeathTexture1,
    enemyDeathTexture2, enemyDeathTexture3, enemyDeathTexture4, enemyDeathTexture5;

var shieldButtonTexture, energyItemTexture, factorItemTexture, shieldItemTexture;
    
var enemyDeathList = [];
var playerDeathList = [];

var enemyDamageList = [];
var playerDamageList = [];

var playerFireList = [];
var enemyFireList = [];

var meteorList = [];

var pBullets = [];
var eBullets = [];
var eShips = [];
var meteors = [];
var radars = [];
var items = [];
var debrisList = [];

var playerFireAudio = new Audio("../assets/sounds/sfx_laser1.ogg");
var enemyFireAudio = new Audio("../assets/sounds/sfx_laser2.ogg");
var deathAudio = new Audio("../assets/sounds/sfx_lose.ogg");
var enemyTransportAudio = new Audio("../assets/sounds/sfx_zap.ogg");
var shieldUpAudio = new Audio("../assets/sounds/sfx_shieldUp.ogg");
var shieldDownAudio = new Audio("../assets/sounds/sfx_shieldDown.ogg");
var damageAudio = new Audio("../assets/sounds/sfx_twoTone.ogg");

var itemTextureList =[];
var randomItemList = [1,1,0,0,1,1,2,2,2,2];
var enemyDropStatus = [0,0,0,0,0,0,0,0,1,1];

var energyItemTimeout, shieldItemTimeout, factorItemTimeout;

var itemColletFunc = {
		0 : function(){
		        energyItem.alpha = 1; 
		        pShip.fillEnergy(); 
		        energyItemTimeout = setTimeout(function(){
		                                energyItem.alpha = 0;
		                                clearTimeout(energyItemTimeout);
		                            },500);},
		1 : function(){
		        shieldItem.alpha = 1; 
		        pShip.getBonusShield(); 
		        shieldItemTimeout = setTimeout(function(){
		                                shieldItem.alpha = 0;
		                                pShip.closeBonusShield();
		                            },20000);},
		2 : function(){
		        factorItem.alpha = 1; 
		        pShip.doubleFactor();
		        factorItemTimeout = setTimeout(function(){
		                                factorItem.alpha = 0; 
		                                pShip.halfFactor();
		                            },20000);}
}

var xStep = x / 10;
var yStep = y / 10;
var vFactorX = xStep / 60;
var vFactorY = yStep / 60;
var antimatter = 5;
var itemID = 0;
var step = x > y ? y / 10 : x / 10;

var windowBounds = {
    x : 0,
    y : 0,
	width : x,
	height : y
};
					
var containerBounds = {
	x : -x,
	y : -y,
	width : 3 * x,
	height : 3 * y
};
					
var rotationParams = {
    f0: {
	        x : 0.5 * x,
			y : 0.4 * y
	},
		
	f1: {
			x : 0.5 * x,
			y : 0.6 * y
	}
};

var rotationList = [0, Math.PI / 2, Math.PI, 3 * Math.PI / 2];

var enemyInterval, meteorInterval, levelTimeout;

function game(){

    calculateRotationParams();
    
    proton = new Proton();
    var renderer = new Proton.PixiRenderer(app.stage);
    proton.addRenderer(renderer);
    
    bgTexture = new PIXI.Texture.fromImage(path + "background.png");
    
    playerShipTexture = new PIXI.Texture.fromImage(path + "playerShip.png");
    enemyShipTexture  = new PIXI.Texture.fromImage(path + "enemyShip.png");
    
    playerLaserTexture = new PIXI.Texture.fromImage(path + "playerLaser.png");
    enemyLaserTexture  = new PIXI.Texture.fromImage(path + "enemyLaser.png");
    
    playerShieldTexture = new PIXI.Texture.fromImage(path + "playerShield1.png");
    playerBonusShieldTexture = new PIXI.Texture.fromImage(path + "playerShield.png");
    enemyRadarTexture   = new PIXI.Texture.fromImage(path + "enemyRadar1.png");
    
    shieldButtonTexture = new PIXI.Texture.fromImage(path + "shieldButton.png");
    
    playerFireTexture0 = new PIXI.Texture.fromImage(path + "playerFire0.png");
    playerFireTexture1 = new PIXI.Texture.fromImage(path + "playerFire1.png");
    playerFireList.push(playerFireTexture0);
    playerFireList.push(playerFireTexture1);
    
    enemyFireTexture0 = new PIXI.Texture.fromImage(path + "enemyFire0.png");
    enemyFireTexture1 = new PIXI.Texture.fromImage(path + "enemyFire1.png");
    enemyFireList.push(playerFireTexture0);
    enemyFireList.push(playerFireTexture1);    
    
    meteorTexture0 = new PIXI.Texture.fromImage(path + "meteorBrown0.png");
    meteorList.push(meteorTexture0);
    meteorTexture1 = new PIXI.Texture.fromImage(path + "meteorBrown1.png");
    meteorList.push(meteorTexture1);
    meteorTexture2 = new PIXI.Texture.fromImage(path + "meteorBrown2.png");
    meteorList.push(meteorTexture2);
    meteorTexture3 = new PIXI.Texture.fromImage(path + "meteorBrown3.png");
    meteorList.push(meteorTexture3);
    meteorTexture4 = new PIXI.Texture.fromImage(path + "meteorGrey0.png");
    meteorList.push(meteorTexture4);
    meteorTexture5 = new PIXI.Texture.fromImage(path + "meteorGrey1.png");
    meteorList.push(meteorTexture5);
    meteorTexture6 = new PIXI.Texture.fromImage(path + "meteorGrey2.png");
    meteorList.push(meteorTexture6);
    meteorTexture7 = new PIXI.Texture.fromImage(path + "meteorGrey3.png");
    meteorList.push(meteorTexture7);
    
    playerDamageTexture0 = new PIXI.Texture.fromImage(path + "playerDamage0.png");
    playerDamageList.push(playerDamageTexture0);
    playerDamageTexture1 = new PIXI.Texture.fromImage(path + "playerDamage1.png");
    playerDamageList.push(playerDamageTexture1);
    playerDamageTexture2 = new PIXI.Texture.fromImage(path + "playerDamage2.png");
    playerDamageList.push(playerDamageTexture2);
    
    enemyDamageTexture0 = new PIXI.Texture.fromImage(path + "enemyDamage0.png");
    enemyDamageList.push(enemyDamageTexture0);
    enemyDamageTexture1 = new PIXI.Texture.fromImage(path + "enemyDamage1.png");
    enemyDamageList.push(enemyDamageTexture1);
    enemyDamageTexture2 = new PIXI.Texture.fromImage(path + "enemyDamage2.png");
    enemyDamageList.push(enemyDamageTexture2);
    
    playerDeathTexture0 = new PIXI.Texture.fromImage(path + "deathPlayer0.png");
    playerDeathList.push(playerDeathTexture0);
    playerDeathTexture1 = new PIXI.Texture.fromImage(path + "deathPlayer1.png");
    playerDeathList.push(playerDeathTexture1);
    playerDeathTexture2 = new PIXI.Texture.fromImage(path + "deathPlayer2.png");
    playerDeathList.push(playerDeathTexture2);
    playerDeathTexture3 = new PIXI.Texture.fromImage(path + "deathPlayer3.png");
    playerDeathList.push(playerDeathTexture3);
    playerDeathTexture4 = new PIXI.Texture.fromImage(path + "deathPlayer4.png");
    playerDeathList.push(playerDeathTexture4);
    playerDeathTexture5 = new PIXI.Texture.fromImage(path + "deathPlayer5.png");
    playerDeathList.push(playerDeathTexture5);
    
    enemyDeathTexture0 = new PIXI.Texture.fromImage(path + "deathEnemy0.png");
    enemyDeathList.push(enemyDeathTexture0);
    enemyDeathTexture1 = new PIXI.Texture.fromImage(path + "deathEnemy1.png");
    enemyDeathList.push(enemyDeathTexture1);
    enemyDeathTexture2 = new PIXI.Texture.fromImage(path + "deathEnemy2.png");
    enemyDeathList.push(enemyDeathTexture2);
    enemyDeathTexture3 = new PIXI.Texture.fromImage(path + "deathEnemy3.png");
    enemyDeathList.push(enemyDeathTexture3);
    enemyDeathTexture4 = new PIXI.Texture.fromImage(path + "deathEnemy4.png");
    enemyDeathList.push(enemyDeathTexture4);
    enemyDeathTexture5 = new PIXI.Texture.fromImage(path + "deathEnemy5.png");
    enemyDeathList.push(enemyDeathTexture5);
    
    energyItemTexture = new PIXI.Texture.fromImage(path + "energyItem.png");
    itemTextureList.push(energyItemTexture);
    shieldItemTexture = new PIXI.Texture.fromImage(path + "shieldItem.png");
    itemTextureList.push(shieldItemTexture);
    factorItemTexture = new PIXI.Texture.fromImage(path + "factorItem.png");
    itemTextureList.push(factorItemTexture);
    
	bg = new PIXI.Sprite(bgTexture);
	bg.x = 1.5 * x;
	bg.y = 1.5 * y;
	bg.anchor.set(0.5);
	bg.width = 3 * x;
	bg.height = 3 * y;
	bg.interactive = true;
	bg.buttonMode = true;
  	bg
		.on('mousedown', touchStart)
		.on('mouseup', touchEnd)
		.on('mousemove', touchMove)
		.on('touchstart', touchStart)
		.on('touchmove', touchMove)
		.on('touchend', touchEnd); 
	
	app.stage.addChild(bg);
	
	container = new PIXI.Container();
	app.stage.addChild(container);
	
	loadLevel("level" + level);
	
	energyCounter = new PIXI.Container();
	energyCounter.position.set(step / 2, step / 2 );
	energyCounter.pivot.x =(step / 4);
	energyCounter.pivot.y =(step / 4);
	app.stage.addChild(energyCounter);
	
	var graphic = new PIXI.Graphics();
	graphic.beginFill(0x343b3d, 0.5);
	graphic.lineStyle(Math.ceil(step / 40), 0xeeeeee, 0.5);
	graphic.drawRoundedRect(0, 0, 2 * step, step / 2, step / 5);
	graphic.endFill();
	energyCounter.addChild(graphic);
	
	var graphic1 = new PIXI.Graphics();
	graphic1.beginFill(0xac3939, 0.5);
	graphic1.drawRoundedRect(0, 0, step, step / 2, step / 5);
	graphic1.endFill();
	energyCounter.addChild(graphic1);
	
	energyStep = (2 * step) / (pShip.maxEnergy);
	
	energyCounter.counter = graphic1;
	
	timerCounter = new PIXI.Container();
	timerCounter.x = x - 0.25 * step;
	timerCounter.y = y - 0.5 * step;
	//timerCounter.pivot.x = (x - 0.25 * step);
	//timerCounter.pivot.y = (y - 0.5 * step);
	timerCounter.rotation = Math.PI;
	timerCounter.alpha = 0.5;
	app.stage.addChild(timerCounter);
	
	var graphic2 = new PIXI.Graphics();
	graphic2.beginFill(0x343b3d, 0.5);
	graphic2.lineStyle(Math.ceil(step / 40), 0xeeeeee, 0.3);
	graphic2.drawRoundedRect(0, 0, 0.5 * step , 2 * step, step / 5);
	graphic2.endFill();
	timerCounter.addChild(graphic2);
	
	var graphic3 = new PIXI.Graphics();
	graphic3.beginFill(0xeeeeec, 0.5);
	graphic3.lineStyle(Math.ceil(step / 40), 0xeeeeee, 0.3);
	graphic3.drawRoundedRect(0, 0, 0.5 * step , 2 * step, step / 5);
	graphic3.endFill();
	timerCounter.addChild(graphic3);
	
	timerStep = (2 * step) / (60 * 180);
	
	timerCounter.counter = graphic3;
	
    var playerShieldButton = new PIXI.Sprite(shieldButtonTexture);
	playerShieldButton.x = x - 0.5 * step;
	playerShieldButton.y = 0.5 * step;
	playerShieldButton.width = step / 2;
	playerShieldButton.height = step / 2;
	playerShieldButton.anchor.set(0.5);
	playerShieldButton.alpha = 0.3;
	playerShieldButton.buttonMode = true;
	playerShieldButton.interactive = true;
	playerShieldButton
			.on('mousedown', openShield)
			.on('mouseup', releaseButton)
			.on('touchstart', openShield)
			.on('touchend', releaseButton);
			
	app.stage.addChild(playerShieldButton);
	
	scoreLabel = new PIXI.Text(scoreText, {fontFamily : "techFontThick", fontSize :  0.5 * step + "px" ,
									        align : "center",  fill : "0xeeeeec", fontWeight: "bold"});
	
	scoreLabel.x = 0.5 * x;
	scoreLabel.y = y - 0.5 * step;
	scoreLabel.anchor.set(0.5);
	scoreLabel.alpha = 0.5;
	
	app.stage.addChild(scoreLabel);
	
	energyItem = new PIXI.Sprite(energyItemTexture);
	energyItem.x = x - 2 * step;
	energyItem.y = 0.5 * step;
	energyItem.width = 0.5 * step;
	energyItem.height = 0.5 * step;
	energyItem.anchor.set(0.5);
	energyItem.alpha = 0;
	
	app.stage.addChild(energyItem);
	
	shieldItem = new PIXI.Sprite(shieldItemTexture);
	shieldItem.x = x - 2.5 * step;
	shieldItem.y = 0.5 * step;
	shieldItem.width = 0.5 * step;
	shieldItem.height = 0.5 * step;
	shieldItem.anchor.set(0.5);
	shieldItem.alpha = 0;
	
	app.stage.addChild(shieldItem);
	
	factorItem = new PIXI.Sprite(factorItemTexture);
	factorItem.x = x - 1.5 * step;
	factorItem.y = 0.5 * step;
	factorItem.width = 0.5 * step;
	factorItem.height = 0.5 * step;
	factorItem.anchor.set(0.5);
	factorItem.alpha = 0;
	
	app.stage.addChild(factorItem);
	
	app.ticker.add(function(){
		
		if(pShip.alive){
			energyCounter.counter.width = pShip.energy * energyStep;
			pShip.move(pShip.dir);
			pShip.moveItems(pShip.dir);
			pShip.moveShield();
			if(pShip.energy > pShip.maxEnergy)
			    pShip.energy = pShip.maxEnery;
		}
		
		timerCounter.counter.height -= timerStep;
		
		debrisList = debrisList.filter(function(debris){
		    var ret = true;
		    
		    if(debris.state == "alive"){
		        debris.move();
		        debris.resolveSelfCrash();
		        debris.recycleDebris();
		    }
		    
		    meteors.forEach(function(meteor){
		        if(hitTestRectangle(debris, meteor)){
		            debris.collide(meteor);
		        }
		    });
		    
		    if(debris.state == "death"){
		        ret = false;
		    }
		    
		    if(hitTestRectangle(debris, pShip) && pShip.alive){
		        pShip.collideDebris(debris);
		    }
		    
		    eShips.forEach(function(eShip){
		        if(eShip.id != debris.id && hitTestRectangle(debris, eShip.radar) && !eShip.teleportStatus){
		            eShip.collideDebris();
		        }
		    });

		    return ret;
		});
		
		meteors = meteors.filter(function(meteor){
			var ret = true;
			meteor.resolveSelfCrash();
			
			if(meteor.strength <= 0){
			    meteor.state = "death";
			}
			
			if(meteor.state == "death"){
				meteor.death();
				ret = false;
			}
			
			if(meteor.state == "free"){
				meteor.move();
				meteor.rotation = (meteor.rotation + 13 * meteor.s) % (Math.PI*2);
			}
						
			if(hitTestRectangle(meteor, pShip)){
				pShip.collide(meteor);
				ret = true;				  
			}

			eShips.forEach(function(eShip){
				if(hitTestRectangle(meteor, eShip)){
					eShip.collide(meteor);
					ret = true;	   
				}				
			});	
			
			return ret;
		});
		
		
		items = items.filter(function(item){
			var ret = true;
			
			if (hitTestRectangle(item, pShip)){
				item.collect();
				ret = false;
			}
			if((item.dropStatus) && (new Date().getTime() - item.dropTime > 5000)){
				container.removeChild(item);
				ret = false;
			}
			
			return ret;
		});
		
		//pBullets = pBullets.reverse();
		pBullets = pBullets.filter(function(bullet){
		
			var ret = true;
			bullet.move();
			
			if(contain(bullet, containerBounds) !== undefined || calculateDistance(bullet, bullet.initialPos) > bullet.range){	   
				container.removeChild(bullet);
				ret = false;
			}
			
			eShips.forEach(function(eShip){
				if(hitTestRectangle(eShip, bullet)){
					ret = eShip.hit(bullet);
					if (ret == undefined) {
						ret = false;
					}
					if (ret === false) {
						container.removeChild(bullet);
					}
				}
			});
			
			meteors.forEach(function(meteor){
				if(hitTestRectangle(meteor, bullet)){
					container.removeChild(bullet);
					ret = false;
				}
			});
			
			return ret;
		});
		
		//eBullets = eBullets.reverse();
		eBullets = eBullets.filter(function(bullet){
		
			var ret = true;
			bullet.move();
			
			if(contain(bullet, containerBounds) !== undefined || calculateDistance(bullet, bullet.initialPos) > bullet.range){	   
				container.removeChild(bullet);
				ret =  false;
			}
			
			if(pShip.shieldStatus && hitTestCircle(pShip.shield, bullet)){
			    container.removeChild(bullet);
			    ret = false;
			}
			
			if(pShip.bonusShieldStatus && hitTestCircle(pShip.bonusShield, bullet)){
			    container.removeChild(bullet);
			    ret = false;			
			}
			
			if(hitTestRectangle(pShip, bullet)){
                ret = pShip.hit(bullet);
				if (ret == undefined) {
					ret = false;
				}
				if (ret === false) {
					container.removeChild(bullet);
				}
			}

			meteors.forEach(function(meteor){
				if(hitTestRectangle(meteor, bullet)){
					container.removeChild(bullet);
					ret = false;
				}
			});
			
			eShips.forEach(function(eShip){
				if(hitTestRectangle(eShip, bullet)){
					bullet.alpha = 0.1;
					//ret = true;
				} else {
					bullet.alpha = 1.0;
					//ret = true;				
				}
			});
			
			return ret;
		});
		
		eShips = eShips.filter(function(eShip){
		    if(!eShip.teleportStatus){
			    eShip.move();
			    eShip.openRadar();
			    eShip.dealTeamCrash();
			}
			
			if(hitTestRectangle(eShip, pShip) && !eShip.teleportStatus){
			    eShip.energy = 0;
			    pShip.energy = 0;
			}
			
			if(eShip.transportStatus){
			    eShip.addAlphaEffect("Invisibility",0);
			}
			
			if(!eShip.transportStatus){
			    eShip.removeAlphaEffect("Invisibility");			
			}
			
			if(eShip.energy <= 0){
				container.removeChild(eShip);
				eShip.death();
				score++;
				updateLabel();
				return false;
			}
			
			return true;
		});
		
		contain(pShip, containerBounds);
		
		if(pShip.energy <= 0 && pShip.alive){
			pShip.death();
			refreshLevel();
		}
		
		proton.update();
		
	});
	
	endGame();
}

function loadLevel(location){
	
	level = location.substring(5);
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "../assets/levels/level" + level + ".json", false);
	xhr.send();

	if (xhr.status < 200 || xhr.status >= 300) {
		console.log("XHR failed.");
		return;
	} else {
		data = JSON.parse(xhr.responseText);
		matrix = data.matrix;
	}
	
	container.removeChild(pShip);
	
	pBullets.forEach(function(element){
		container.removeChild(element);
	});
	pBullets.length = 0;
	
	eBullets.forEach(function(element){
		container.removeChild(element);
	});
	eBullets.length = 0;
	
	eShips.forEach(function(element){
		container.removeChild(element);
	});
	eShips.length = 0;
	
	meteors.forEach(function(element){
		container.removeChild(element);
	});
	meteors.length = 0;
	
	radars.forEach(function(element){
		container.removeChild(element);
	});
	radars.length = 0;
	
	items.forEach(function(element){
		container.removeChild(element);
	});
	items.length = 0;
	
	debrisList.forEach(function(element){
		container.removeChild(element);
	});
	debrisList.length = 0;
	
	for(var i = 0; i < matrix.length; i++){
		for(var j = 0; j < matrix[0].length; j++){
			itemID++;
			if(matrix[i][j] == 'M'){
				var meteor = new Meteor(meteorTexture0);
				meteor.id = itemID;
				meteor.x = (j-10) * step + 0.5 * step;
				meteor.y = (i-10) * step + 0.5 * step;
				meteor.width  = step;
				meteor.height = step;
				meteor.s = Math.PI/3600;
				meteor.calculateRotParams();
				meteor.state = "free";
				meteors.push(meteor);
				
				container.addChild(meteor);
			} else if (matrix[i][j] == 'W') {
				var meteor = new Meteor(meteorTexture0);
				meteor.id = itemID;
				meteor.x = (j-10) * step + 0.5 * step;
				meteor.y = (i-10) * step + 0.5 * step;
				meteor.width  = step;
				meteor.height = step;
				meteor.s = -Math.PI/3600;

				meteor.calculateRotParams();
				meteor.state = "free";
				meteors.push(meteor);
				
				container.addChild(meteor);
			}
			else if(matrix[i][j] == 'P'){
				pShip = new playerShip(playerShipTexture);
				pShip.id = itemID;
				pShip.width  = step;
				pShip.height = step;
				pShip.updateFactor(2);
				pShip.fillEnergy();
				pShip.x = (j-10) * step + 0.5 * step;
				pShip.y = (i-10) * step + 0.5 * step;
				pShip.bulletTextures["energyBullet"] = playerLaserTexture;
				pShip.deathItemsTextures = playerDeathList;
				pShip.damageEffects = playerDamageList;
				pShip.fireEffects = playerFireList;
				container.addChild(pShip);
				
				container.x = x / 2 - pShip.x;
				container.y = y / 2 - pShip.y;
				if (container.x < -x) {
					container.x = -x;
				} else if (container.x > x) {
					container.x = x;
				}
				if (container.y < -y) {
					container.y = -y;
				} else if (container.y > y) {
					container.y = y;
				}
				
				var shield = new Shield(playerShieldTexture, pShip, 0);
				shield.width = 2 * step;
				shield.height = 2 * step;
				shield.x = pShip.x;
				shield.y = pShip.y;
				shield.alpha = 0;
				pShip.shield = shield;
				
				container.addChild(shield);
				
				var bonusShield = new Shield(playerBonusShieldTexture, pShip, 0);
				bonusShield.width = 2 * step;
				bonusShield.height = 2 * step;
				bonusShield.x = pShip.x;
				bonusShield.y = pShip.y;
				bonusShield.alpha = 0;
				pShip.bonusShield = bonusShield;
				
				container.addChild(bonusShield);	
				
			}else if(matrix[i][j] == '1'){
				var enemy = new Enemy(enemyShipTexture);
				enemy.id = itemID;
				enemy.width = step;
				enemy.height = step;
				enemy.updateFactor(difficultyStatus + 1);
				enemy.fillEnergy();
				enemy.x = (j-10) * step + 0.5 * step;
				enemy.y = (i-10) * step + 0.5 * step;
				enemy.rotation = j > 5 ?  (Math.PI / 2) : (3 * Math.PI / 2);
				enemy.bulletTextures["energyBullet"] = enemyLaserTexture;
				enemy.deathItemsTextures = enemyDeathList;
				enemy.damageEffects = enemyDamageList;
				enemy.fireEffects = enemyFireList;
				enemy.type = 1;
				eShips.push(enemy);
				
				container.addChild(enemy);
				
				var radar = new Radar(enemyRadarTexture, enemy, 0);
				radar.width = (3 + difficultyStatus) * step;
				radar.height = (3 + difficultyStatus) * step;
				radar.x = enemy.x;
				radar.y = enemy.y;
				enemy.radar = radar;
				
				radars.push(radar);
				container.addChild(radar);
				
				var rnd = randomInt(0, 10);
				var rndItem = randomItemList[rnd];
				var status  = enemyDropStatus[rnd];
				var item = new Item(itemTextureList[rndItem], itemColletFunc[rndItem]);
				enemy.drop = status;
				enemy.item = item;
				
				items.push(item);		  
			}
			else if(matrix[i][j] == '2'){
				var enemy = new Enemy(enemyShipTexture);
				enemy.id = itemID;
				enemy.width = step;
				enemy.height = step;
				enemy.updateFactor(difficultyStatus + 1);
				enemy.fillEnergy();
				enemy.x = (j-10) * step + 0.5 * step;
				enemy.y = (i-10) * step + 0.5 * step;
				enemy.rotation = j > 5 ?  (Math.PI) : 0;
				enemy.bulletTextures["energyBullet"] = enemyLaserTexture;
				enemy.deathItemsTextures = enemyDeathList;
				enemy.damageEffects = enemyDamageList;
				enemy.fireEffects = enemyFireList;
				enemy.type = 2;
				eShips.push(enemy);
				
				container.addChild(enemy);
				
				var radar = new Radar(enemyRadarTexture, enemy, 0);
				radar.width = (3 + difficultyStatus) * step;
				radar.height = (3 + difficultyStatus) * step;
				radar.x = enemy.x;
				radar.y = enemy.y;
				enemy.radar = radar;
				
				radars.push(radar);
				container.addChild(radar);
				
				var rnd = randomInt(0, 10);
				var rndItem = randomItemList[rnd];
				var status  = enemyDropStatus[rnd];
				var item = new Item(itemTextureList[rndItem], itemColletFunc[rndItem]);
				enemy.drop = status;
				enemy.item = item;
				
				items.push(item);	
				
			}
		}
	}
		
	enemyInterval = setInterval(function(){
	    if(eShips.length <= 30){
	        var newEnemyX = randomInt(-x + step, 2 * x - step);
	        var newEnemyY = randomInt(-y + step, 2 * y - step);
	        createEnemy(newEnemyX, newEnemyY);
	    }
	            
	},5000);
	
	meteorInterval = setInterval(function(){
	    if(meteors.length <= 50){
	        var newMeteorX = randomInt(-x + step, 2 * x - step);
	        var newMeteorY = randomInt(-y + step, 2 * y - step);
	        createMeteor(newMeteorX, newMeteorY);
	    }	
	}, 5000);
	
	//updateLabel();	
}

window.onload = function(){
	
	app = new PIXI.Application(x, y, {backgroundColor : 0xFFFFFF});
	document.body.appendChild(app.view);
	game();

}
