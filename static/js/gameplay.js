function updateLabel(){
    scoreText = scorePrompt + score;
    scoreLabel.text = scoreText;
}

function updateBar(){
    energyCounter.counter.width = pShip.energy * energyStep;
    timerCounter.counter.height = 2 * step;
}

function createEnemy(pointX, pointY, id = itemID++){

    var enemy = new Enemy(enemyShipTexture);
    enemy.id = id;
	enemy.width = step;
	enemy.height = step;
	enemy.updateFactor(difficultyStatus + 1);
	enemy.x = pointX;
	enemy.y = pointY;
	enemy.alpha = 1;
	enemy.rotation = rotationList[randomInt(0,4)];
	enemy.bulletTextures["energyBullet"] = enemyLaserTexture;
	enemy.deathItemsTextures = enemyDeathList;
	enemy.damageEffects = enemyDamageList;
	enemy.fireEffects = enemyFireList;
	
	var val = eShips.some(function(eShip){
	            var ret = false;
	            if(hitTestRectangle(enemy, eShip)){
	                ret = true;
	            }
	            return ret;
	        });
	eShips.push(enemy);
	container.addChild(enemy);
	if(val){
	    console.log("transport...");
	    var obj = enemy.calculateTeleportPoint();
	    enemy.x = obj.x;
	    enemy.y = obj.y;
	    enemy.transportStatus = true;
	    enemy.transportTimeout = setTimeout(function(){
	                                enemy.transportStatus = false;
	                                clearTimeout(enemy.transportTimeout);   
	                             }, 1000);
	}else{
	    console.log("not transport...");
	}
				
	
				
	var radar = new Radar(enemyRadarTexture, enemy, 0);
	radar.width = (3 + difficultyStatus) * step;
	radar.height = (3 + difficultyStatus) * step;
	radar.x = enemy.x;
	radar.y = enemy.y;
	enemy.radar = radar;
				
	radars.push(radar);
	container.addChild(radar);
	
	var rnd = randomInt(0, 10);
	var rndItem = randomItemList[rnd];
	var status  = enemyDropStatus[rnd];
	var item = new Item(itemTextureList[rndItem], itemColletFunc[rndItem]);
	enemy.drop = status;
	enemy.item = item;
				
	items.push(item);
}

function createMeteor(pointX, pointY){

    var meteorTypeList = ["M", "W"];
    var meteorType = meteorTypeList[randomInt(0,2)];

	var meteor = new Meteor(meteorTexture0);
	meteor.id = itemID++;
	meteor.x = pointX;
	meteor.y = pointY;
	meteor.width  = step;
	meteor.height = step;
	meteor.s = meteorType == "M" ? Math.PI/3600 : -Math.PI/3600;
	meteor.calculateRotParams();
	meteor.state = "free";
	meteors.push(meteor);
	
	container.addChild(meteor);

}

function refreshLevel(){
    if(score * Math.pow(10, difficultyStatus) + pShip.energy > maxScore){
        maxScore = score * Math.pow(2, difficultyStatus) + pShip.energy;
    }
    
	score = 0;
	
	clearInterval(enemyInterval);
	clearInterval(meteorInterval);
	clearTimeout(levelTimeout);
	
	container.removeChildren();
	
	if(energyItem.alpha == 1){
	    energyItem.alpha = 0;
	    clearTimeout(energyItemTimeout);
	}
	
	if(factorItem.alpha == 1){
        factorItem.alpha = 0; 
		pShip.halfFactor();	
	}
	
	if(shieldItem.alpha == 1){
	    shieldItem.alpha = 0;
	    pShip.closeBonusShield();
	}
	
	loadLevel("level" + level);
	updateBar();
	updateLabel();
	endGame();
}

function endGame(){
    levelTimeout = setTimeout(function(){
                        refreshLevel();
                   }, 180000);
}
