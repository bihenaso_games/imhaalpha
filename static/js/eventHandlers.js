function touchStart(event){
	
	this.data = event.data;
	this.flag = true;
	
	this.startX = this.data.getLocalPosition(this.parent).x;
	this.startY = this.data.getLocalPosition(this.parent).y;
	
	if(this.startY < y / 2){
        pShip.eFire();
	}
}
		
function touchEnd(){
	
	this.flag = false;
	this.data = null;
	
	pShip.dir = 0;
}
		
function touchMove(event){
	
	if(this.flag && this.startY >= y / 2 && this.startY < y){ 
																	
		this.curX = this.data.getLocalPosition(this.parent).x;
		this.curY = this.data.getLocalPosition(this.parent).y;
		
		pShip.beforeRotation = pShip.rotation;
		
		if (this.curX - this.startX > 0 && Math.abs(this.curX-this.startX)>Math.abs(this.curY-this.startY)) {
			pShip.dir = 3;
		}
		if(this.curX - this.startX < 0 && Math.abs(this.curX-this.startX)>Math.abs(this.curY-this.startY)){
			pShip.dir = 4;				
		}
		if(this.curY - this.startY > 0 && Math.abs(this.curX-this.startX)<Math.abs(this.curY-this.startY)){
			pShip.dir = 1;						
		}
		if(this.curY - this.startY < 0 && Math.abs(this.curX-this.startX)<Math.abs(this.curY-this.startY)){
			pShip.dir = 2;				
		}
	}
}

function openShield(event){

	this.data = event.data;
	this.flag = true;
	
	if(!pShip.shieldStatus){
	    if(soundStatus) shieldUpAudio.play();
		pShip.shieldStatus = true;
		pShip.shield.alpha = 1;
		
	}else{
	    if(soundStatus) shieldDownAudio.play();
		pShip.shieldStatus = false;
		pShip.shield.alpha = 0;
	}
}

function releaseButton(){
	this.data = null;
	this.flag = false;
}

function onKeyDown(key){

    pShip.beforeRotation = pShip.rotation;

    if(key.keyCode == 87 || key.keyCode == 38){
        pShip.dir = 2;
    }
    
    if (key.keyCode == 83 || key.keyCode == 40){
        pShip.dir = 1;
    }
    
    if(key.keyCode == 65 || key.keyCode == 37){
        pShip.dir = 4;
    }
    
    if (key.keyCode == 68 || key.keyCode == 39){
        pShip.dir = 3;
    }
    
    if(key.keyCode == 13){
        pShip.eFire();
    }
    
    if(key.keyCode == 8){
        localStorage.setItem("soundStatus", JSON.stringify(soundStatus));
		localStorage.setItem("difficultyStatus", JSON.stringify(difficultyStatus));
		localStorage.setItem("maxScore", JSON.stringify(maxScore));
        window.location.assign("../index.html");
    }
}

function onKeyUp(){
    pShip.dir = 0;
}
