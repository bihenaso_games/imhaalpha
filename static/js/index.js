if (typeof(Storage) !== "undefined") {
    if(!localStorage.getItem("soundStatus")){
        localStorage.setItem("soundStatus", JSON.stringify(true));
    }
    if(!localStorage.getItem("difficultyStatus")){
	    localStorage.setItem("difficultyStatus", JSON.stringify(0));
	}
    if(!localStorage.getItem("maxScore")){
	    localStorage.setItem("maxScore", JSON.stringify(0));
	}
} else {
	console.log("No local storage.")
}

var w = window,
	d = document,
	e = d.documentElement,
	g = d.getElementsByTagName('body')[0],
	x = w.innerWidth || e.clientWidth || g.clientWidth,
	y = w.innerHeight|| e.clientHeight|| g.clientHeight;

var orient = (x >= y) ? 'l' : 'p';
var ref = calculateSize();
var y_begin = y / 20;
var y_end = y - y_begin;
var y_step = y * 0.15;
var x_step = ref / 4;
var emptySpace = ref / 5;
var x_begin = x / 20;

var soundStatus = JSON.parse(localStorage.getItem("soundStatus"));
var difficultyStatus = JSON.parse(localStorage.getItem("difficultyStatus"));
var maxScore = JSON.parse(localStorage.getItem("maxScore"));

var scorePrompt = "MAX SCORE: ";
var scoreText = scorePrompt + maxScore;

var app;
var graphics = new PIXI.Graphics();
var audio = new Audio("./assets/sounds/button.wav");

level = 0;

var path = "./assets/images";

var u_button, m_button;

function menuPage(){
	
	stepX = x / 10;
	stepY = y / 10;
	
	stepInX = (6 * stepX) / 12;
	stepInY = (6 * stepY) / 12;
	
	bgTexture = PIXI.Texture.fromImage(path + "/index_bg.png");
	bg = new PIXI.Sprite(bgTexture);
	bg.anchor.x = 0.5;
	bg.anchor.y = 0.5;
	bg.x = x/2;
	bg.y = y/2;
	if (x > y) {
		bg.height = x;
		bg.width = x;
	} else {
		bg.width = y;
		bg.height = y;
	}
	app.stage.addChild(bg);
	
	var texture = PIXI.Texture.fromImage(path + "/play.png");

	
	var texture6 = PIXI.Texture.fromImage(path + "/mute.png");
	var texture7 = PIXI.Texture.fromImage(path + "/unmute.png");
	
	var texture8 = PIXI.Texture.fromImage(path + "/easy.png");
	var texture9 = PIXI.Texture.fromImage(path + "/medium.png");
	var texture10 = PIXI.Texture.fromImage(path + "/hard.png");
	
	var c_button = new PIXI.Sprite(texture);
	
	m_button = new PIXI.Sprite(texture6);
	u_button = new PIXI.Sprite(texture7);
	
	easy_button = new PIXI.Sprite(texture8);
	medium_button = new PIXI.Sprite(texture9);
	hard_button = new PIXI.Sprite(texture10);
	

	scoreLabel = new PIXI.Text(scoreText, {fontFamily : "techFontThick", fontSize :  0.5 * ref + "px" ,
									        align : "center",  fill : "0xeeeeec", fontWeight: "bold"});
	
	scoreLabel.x = 0.5 * x;
	scoreLabel.y = y_begin + y_step + emptySpace;
	scoreLabel.anchor.set(0.5);
	scoreLabel.alpha = 0.5;
	
	app.stage.addChild(scoreLabel);
	

	c_button.position.x = x / 2;
	c_button.position.y = y_begin + 2 * y_step + 2 * emptySpace;
	c_button.width = ref;
	c_button.height = ref;
	c_button.anchor.x = 0.5;
	c_button.buttonMode = true;
	c_button.interactive = true;
	c_button.id = 0;
	c_button
			.on('mousedown', onButtonDown)
			.on('mouseup', onButtonUp)
			.on('touchstart', onButtonDown)
			.on('touchend', onButtonUp)
			
	app.stage.addChild(c_button);
	
	
	u_button.position.x = c_button.x - (c_button.width / 2) - 3 * x_step;
	u_button.position.y = y_begin + 3 * y_step + 3 * emptySpace;
	u_button.width = ref;
	u_button.height = ref;
	u_button.interactive = true;
	u_button.buttonMode = true;
	u_button.id = 0;
	u_button
			.on('mousedown', muteFunc)
			.on('mouseup', muteFunc)
			.on('touchstart', muteFunc)
			.on('touchend', muteFunc)
	
	app.stage.addChild(u_button);
	
	m_button.position.x = c_button.x - (c_button.width / 2) - 3 * x_step;
	m_button.position.y = y_begin + 3 * y_step + 3 * emptySpace;
	m_button.width = ref;
	m_button.height = ref;
	m_button.interactive = true;
	m_button.buttonMode = true;
	m_button.id = 1;
	m_button
			.on('mousedown', unmuteFunc)
			.on('mouseup', unmuteFunc)
			.on('touchstart', unmuteFunc)
			.on('touchend', unmuteFunc)
	
	app.stage.addChild(m_button);
			
	easy_button.position.x = c_button.x - (c_button.width / 2) + 3 * x_step;
	easy_button.position.y = y_begin + 3 * y_step + 3 * emptySpace;
	easy_button.width = ref;
	easy_button.height = ref;
	easy_button.interactive = true;
	easy_button.buttonMode = true;
	easy_button.id = 1;
	easy_button
			.on('mousedown', easy)
			.on('mouseup', easy)
			.on('touchstart', easy)
			.on('touchend', easy)
	app.stage.addChild(easy_button);
	
	medium_button.position.x = c_button.x - (c_button.width / 2) + 3 * x_step;
	medium_button.position.y = y_begin + 3 * y_step + 3 * emptySpace;
	medium_button.width = ref;
	medium_button.height = ref;
	medium_button.interactive = true;
	medium_button.buttonMode = true;
	medium_button.id = 1;
	medium_button
			.on('mousedown', medium)
			.on('mouseup', medium)
			.on('touchstart', medium)
			.on('touchend', medium)
	app.stage.addChild(medium_button);
			
	hard_button.position.x = c_button.x - (c_button.width / 2) + 3 * x_step;
	hard_button.position.y = y_begin + 3 * y_step + 3 * emptySpace;
	hard_button.width = ref;
	hard_button.height = ref;
	hard_button.interactive = true;
	hard_button.buttonMode = true;
	hard_button.id = 1;
	hard_button
			.on('mousedown', hard)
			.on('mouseup', hard)
			.on('touchstart', hard)
			.on('touchend', hard)
	
	app.stage.addChild(hard_button);

	
	if(soundStatus === true){
	
		m_button.visible = false;
		m_button.interactive = false;
		m_button.buttonMode = false;
		
		u_button.visible = true;
		u_button.interactive = true;
		u_button.buttonMode = true;
	}else{
	
		u_button.visible = false;
		u_button.interactive = false;
		u_button.buttonMode = false;
		
		m_button.visible = true;
		m_button.interactive = true;
		m_button.buttonMode = true;
	}
	
	if(difficultyStatus === 0){
		easy_button.visible = true;
		easy_button.interactive = true;
		easy_button.buttonMode = true;
		
		medium_button.visible = false;
		medium_button.interactive = false;
		medium_button.buttonMode = false;
		
		hard_button.visible = false;
		hard_button.interactive = false;
		hard_button.buttonMode = false;
		
	}else if(difficultyStatus === 1){
		easy_button.visible = false;
		easy_button.interactive = false;
		easy_button.buttonMode = false;
		
		medium_button.visible = true;
		medium_button.interactive = true;
		medium_button.buttonMode = true;
		
		hard_button.visible = false;
		hard_button.interactive = false;
		hard_button.buttonMode = false;
	}else{
		easy_button.visible = false;
		easy_button.interactive = false;
		easy_button.buttonMode = false;
		
		medium_button.visible = false;
		medium_button.interactive = false;
		medium_button.buttonMode = false;
		
		hard_button.visible = true;
		hard_button.interactive = true;
		hard_button.buttonMode = true;
	}
	
}

function onButtonDown(event) {
	this.data = event.data;
	this.isdown = true;
	this.alpha = 0.5;
    if(soundStatus) audio.play();
    setTimeout(function(){
        window.location.assign("./html/game.html?level=" + level);
    },500);
}

function muteFunc(){
	soundStatus = false;
	updateAllStatus();
		
	u_button.visible = false;
	m_button.visible = true;
	
	u_button.interactive = false;
	u_button.buttonMode = false;	
	
	setTimeout(function(){
		m_button.interactive = true;
		m_button.buttonMode = true;
	},100);
}

function unmuteFunc(){	
	soundStatus = true;
	updateAllStatus();
	
	m_button.visible = false;
	u_button.visible = true;
		
	m_button.interactive = false;
	m_button.buttonMode = false;
				
	setTimeout(function(){
		u_button.interactive = true;
		u_button.buttonMode = true;
		audio.play();
	},100);	   
}

function easy(){
	if(soundStatus) audio.play();
	
	difficultyStatus = 1;
	updateAllStatus();
	
	easy_button.visible = false;
	medium_button.visible = true;
	hard_button.visible = false;
	
	easy_button.interactive = false;
	easy_button.buttonMode = false;
	hard_button.interactive = false;
	hard_button.buttonMode = false;
	
	setTimeout(function(){
		medium_button.interactive = true;
		medium_button.buttonMode = true;
	},100);
}

function medium(){
	if(soundStatus) audio.play();
	
	difficultyStatus = 2;
	updateAllStatus();
	
	easy_button.visible = false;
	medium_button.visible = false;
	hard_button.visible = true;
	
	easy_button.interactive = false;
	easy_button.buttonMode = false;
	medium_button.interactive = false;
	medium_button.buttonMode = false;
	
	setTimeout(function(){
		hard_button.interactive = true;
		hard_button.buttonMode = true;
	},100);
}

function hard(){
	if(soundStatus) audio.play();
	
	difficultyStatus = 0;
	updateAllStatus();
	
	easy_button.visible = true;
	medium_button.visible = false;
	hard_button.visible = false;
	
	medium_button.interactive = false;
	medium_button.buttonMode = false;
	hard_button.interactive = false;
	hard_button.buttonMode = false;	

	setTimeout(function(){
		easy_button.interactive = true;
		easy_button.buttonMode = true;
	},100);
}

function onButtonUp() {
	this.isdown = false;
	this.alpha = 1.0;
}

function calculateSize(){
	var ref = y * 0.15;
	var ref1 = ref * 4;
	if(ref1 > x * 0.9){
		ref -= (ref1 - (x * 0.9)) / 4;
	}
	
	return ref;
}

function updateAllStatus(){
	localStorage.setItem("difficultyStatus", JSON.stringify(difficultyStatus));
	localStorage.setItem("soundStatus", JSON.stringify(soundStatus));
	localStorage.setItem("maxScore", JSON.stringify(maxScore));
}

window.onload = function(){
	
	app = new PIXI.Application(x, y, {backgroundColor : 0x2e3436, resolution : window.devicePixelRatio});
	document.body.appendChild(app.view);
	menuPage();
}
