class Item extends PIXI.Sprite{
	constructor(texture, collectFunc = null, dropTime = null){
		super(texture);
		this.anchor.set(0.5);
		this.collectFunc = collectFunc;
		this.dropTime = dropTime;
		this.dropStatus = false;
	}
	
	collect(){
		this.collectFunc();
		container.removeChild(this);
	}
}


class bullet extends PIXI.Sprite{
	constructor(texture, ship, range = 100){
		super(texture);
		this.anchor.set(0.5);
		this.ship = ship;
		this.initialPos = {
			x: this.x,
			y: this.y
		};
		this.range = range;
	}
}

class energyBullet extends bullet{
	constructor(texture, ship, range = 100){
		super(texture, ship, range);
		this.rotation = this.ship.rotation;
		this.factor = this.ship.factor;
		this.effect = 10 * this.factor;
		this.speedX = 2 * this.factor * vFactorX;
		this.speedY = 2 * this.factor * vFactorY;
	}
	
	move(){
		this.x += this.speedX * Math.sin(this.rotation);
		this.y -= this.speedY * Math.cos(this.rotation);			   
	}			
}

class Ship extends PIXI.Sprite{

	constructor(texture, energy = 100, factor = 1){
		super(texture);
		this.factor = factor;
		this.speedX = this.factor * vFactorX;
		this.speedY = this.factor * vFactorY;
		this.energy = energy;
		this.anchor.set(0.5);
	}
	
	updateFactor(factor){
	    this.factor = factor;
	    this.speedX = this.factor * vFactorX;
		this.speedY = this.factor * vFactorY;
	    this.maxEnergy = this.factor * 100;
	}
	
	fillEnergy(factor){
	    this.energy = this.maxEnergy;
	}
	
}

class Debris extends PIXI.Container{

    constructor(x, y, itemList, factor){
        super();
        this.x = x;
        this.y = y;
        this.itemList = itemList;
        this.factor = factor;
        this.t = 0;
        this.a = 0;
        this.b = 0;
        this.s = 0;
        this.id = null;
        this.state = "alive";
        this.move = Debris.defaultMove.bind(this);
        this.createTime = null;
    }
    
    createDebris(){
    	this.removeChild.apply(this,this.children);
		this.itemList.forEach(function(texture){
			var sprite = new PIXI.Sprite(texture);
			sprite.x = randomInt(0, step * this.factor);
			sprite.y = randomInt(0, step * this.factor);
			sprite.width = step / 2;
			sprite.height = step / 2;
			sprite.anchor.set(0.5);
			this.damage = 20 * this.factor;
			this.addChild(sprite);
		}.bind(this));
		
		this.pivot.x = this.width / 2;
        this.pivot.y = this.height / 2;
        
        container.removeChild(this);
		container.addChild(this);
		this.createTime = new Date().getTime();      
    }
    
    recycleDebris(){
        var tempTime = new Date().getTime();
        if(tempTime - this.createTime > 5000 && contain(this, containerBounds) == undefined){
            console.log("debris recycle");
            this.removeChild.apply(this,this.children);
            createEnemy(this.x, this.y, this.id);	
			this.state ="death";
			container.removeChild(this);	      
        }
    }
    
	calculateRotParams() {
		this.t = Math.atan2(this.y - rotationParams.o.y, this.x - rotationParams.o.x) + rotationParams.e;
		
		var m_t = {
			x : (this.x-rotationParams.o.x)*rotationParams.cos_e + (this.y-rotationParams.o.y)*rotationParams.sin_e,
			y : (this.y-rotationParams.o.y)*rotationParams.cos_e + (this.x-rotationParams.o.x)*rotationParams.sin_e
		};
		
		var c_2 = (((rotationParams.f0_t_x - rotationParams.f1_t_x) ** 2) / 4);
		var temp = (m_t.x ** 2) + (m_t.y ** 2) - c_2;
		
		var b_2 = ( temp + Math.sqrt( temp ** 2 + 4*c_2*(m_t.y ** 2)) ) / 2;
		this.b = Math.sqrt(b_2);
		this.a = Math.sqrt(b_2 + c_2);
	}
    
	static defaultMove() {
		this.t = (this.t + this.s) % (2*Math.PI);
		
		var m_t = {
			x : this.a * Math.cos(this.t),
			y : this.b * Math.sin(this.t)
		};
		
		this.x = m_t.x*rotationParams.cos_e + m_t.y*rotationParams.sin_e + rotationParams.o.x;
		this.y = m_t.y*rotationParams.cos_e - m_t.x*rotationParams.sin_e + rotationParams.o.y;
	}
	
	resolveSelfCrash(){
	
		debrisList.forEach(function(debris){
			if(hitTestRectangle(this, debris) && this.id != debris.id){
                this.itemList = this.itemList.concat(debris.itemList);
                this.factor = this.factor + debris.factor;
		        this.createDebris();
		        this.calculateRotParams();
		        debris.x = -3 * x;
		        debris.y = -3 * y;
				debris.state = "death";
				container.removeChild(debris);
			}else{
			
			}
		}.bind(this));  
	}
	
	collide(meteor){
		var slope = Math.PI / 2 + calculateSlope(this, meteor);
		
		meteorCrashEffect(container.x + meteor.x, container.y + meteor.y, (180/Math.PI) * (Math.PI + slope), 1);
		
		this.x += step * Math.sin(slope);
		this.y -= step * Math.cos(slope);
		
		meteor.strength -= this.damage;	
	}

}

class playerShip extends Ship{

	constructor(texture, energy = 100, factor = 1, itemList = {}, 
					bulletTextures = {}, deathItemsTextures = [], damageEffects = [], fireEffects = [], rockets = {}, shield = null, bonusShield = null){
		super(texture, energy, factor);
		this.itemList = itemList;
		this.bulletTextures = bulletTextures;
		this.deathItemsTextures = deathItemsTextures;
		this.damageEffects = damageEffects;
		this.fireEffects = fireEffects;
		this.rockets = rockets;
		this.shield = shield;
		this.bonusShield = bonusShield;
		this.readyLauncher = true;
		this.timeoutId = 0;
		this.alive = true;
		this.actionStatus = null;
		this.dir = 0;
		this.beforeRotation = 0;
		this.anim = null;
		this.animTimeout = null;
		this.shieldStatus = false;
		this.bonusShieldStatus = false;
		this.maxEnergy = this.energy * this.factor
	}
	
	hit(bullet){
	    if(this.energy > bullet.effect){
		    this.bulletHitAnim(bullet);
		}
		if(soundStatus) damageAudio.play();
		this.energy -= bullet.effect;
	}
	
	move(direction){
		if(direction == 1){
			this.rotation = Math.PI;
			this.shield.rotation = Math.PI;
			this.bonusShield.rotation = Math.PI;
			if (container.y > -y && this.y + container.y >= y / 2) {
				container.y -= this.speedY;
			}
			this.y += this.speedY;
			
		}
		
		if(direction == 2){
			this.rotation = 0;
			this.shield.rotation = 0;
			this.bonusShield.rotation = 0;
			if (container.y < y && this.y + container.y <= y / 2) {
				container.y += this.speedY;
			}
			this.y -= this.speedY;
		}
		
		if(direction == 3){
			this.rotation = Math.PI / 2;
			this.shield.rotation = Math.PI / 2;
			this.bonusShield.rotation = Math.PI / 2;
			if (container.x > -x && this.x + container.x >= x/2) {
				container.x -= this.speedX;
			}
			this.x += this.speedX;
		}
		
		if(direction == 4){
			this.rotation = 3 * Math.PI / 2;
			this.shield.rotation = 3 * Math.PI / 2;
			this.bonusShield.rotation = 3 * Math.PI / 2;
			if (container.x < x && this.x + container.x <= x/2) {
				container.x += this.speedX;
			}
			this.x -= this.speedX;
		}
		
		if(this.anim !== null){
		    this.anim.x = this.x;
		    this.anim.y = this.y;
		}
		if(this.dir && this.changeDirection() == "Y"){
			var motor = this.getMotor1();
			var motor1 = this.getMotor2();
			playerMotors(motor.x + container.x, motor.y + container.y, (180/Math.PI) * (this.rotation + Math.PI/2));
			playerMotors(motor1.x + container.x, motor1.y + container.y, (180/Math.PI) * (this.rotation - Math.PI/2));
		}
		if(this.dir && this.changeDirection() == "X"){
			var motor = this.getMotor0();
			var motor1 = this.getMotor3();
			playerMotors(motor.x + container.x, motor.y + container.y, (180/Math.PI) * (this.rotation - Math.PI/2));
			playerMotors(motor1.x + container.x, motor1.y + container.y, (180/Math.PI) * (this.rotation + Math.PI/2));
		}
		
		
	}
	
	moveShield(){
	    this.shield.x = this.x;
	    this.shield.y = this.y;
	    
	    this.bonusShield.x = this.x;
	    this.bonusShield.y = this.y;
	}
	
	moveItems(direction){
		var breakerLoc = this.getBreakerLoc();
		Object.values(this.itemList).forEach(function(obj){
				if(obj){
					obj.rotation = this.rotation;
					obj.x = breakerLoc.x + obj.distance * Math.sin(this.rotation);
					obj.y = breakerLoc.y - obj.distance * Math.cos(this.rotation); 
				}
		}.bind(this));
	}
	
	changeDirection(direction){
		var temp = this.beforeRotation - this.rotation;
		if(temp == 0){
			return false;
		}
		else if (temp > Math.PI || temp < 0 && temp > -Math.PI){
			return "X";
		} else {
			return "Y";
		}
	}
	
	eFire(){
		if(this.readyLauncher && this.alive){
            var barrelLoc = this.getBreakerLoc();
            if(soundStatus) playerFireAudio.play();
			var eBullet = new energyBullet(this.bulletTextures["energyBullet"], this);
			eBullet.width = step / 8;
			eBullet.height = step / 2;
			eBullet.x = barrelLoc.x;
			eBullet.y = barrelLoc.y;
			eBullet.initialPos.x = this.x;
			eBullet.initialPos.y = this.y;
			eBullet.range = 10 * step;
			eBullet.alpha = this.alpha;
			container.addChild(eBullet);
			pBullets.push(eBullet);
			this.readyLauncher = false;
			this.timeoutId = setTimeout(function(){
			    this.readyLauncher = true;
			    clearTimeout(this.timeoutId);
			}.bind(this), 1000);
		}
	}
	
	death(){
		
	    bulletCrashEffect(container.x + this.x, container.y + this.y, (180/Math.PI) * (this.rotation), 2, 0);
	    if(soundStatus) deathAudio.play();
	    
		this.deathItemsTextures.forEach(function(texture){
			var sprite = new PIXI.Sprite(texture);
			sprite.x = randomInt(this.x - this.width / 2, this.x + this.width /2);
			sprite.y = randomInt(this.y - this.height / 2, this.y + this.height /2);
			sprite.width = step / 2;
			sprite.height = step / 2;
			sprite.anchor.set(0.5);
			container.addChild(sprite);
		}.bind(this));
		
		this.alive = false;
		this.shieldStatus = false;
		this.shield.alpha = 0;
		this.x = -4 * x;
		this.y = -4 * y;
		this.shield.x = -4 * x;
		this.shield.y = -4 * y;
		container.removeChild(this.shield);
		container.removeChild(this);
		energyCounter.counter.width = 0;
	}
	
	getBreakerLoc(){
		if(this.rotation == Math.PI){
			return {x : this.x, y : this.y + this.height / 2};
		}
		
		else if(this.rotation == 0){
			return {x : this.x, y : this.y - this.height / 2};
		}
		else if(this.rotation == Math.PI / 2){
			return {x : this.x + this.width / 2, y : this.y};
		}
		else if(this.rotation == 3 * Math.PI / 2){
			return {x : this.x - this.width / 2, y : this.y};
		}
	}
	
	getMotor0(){
		if(this.rotation == Math.PI){
			return {x : this.x + this.width / 2, y : this.y + this.height / 2};
		}
		
		else if(this.rotation == 0){
			return {x : this.x - this.width / 2, y : this.y - this.height / 2};
		}
		else if(this.rotation == Math.PI / 2){
			return {x : this.x + this.width / 2, y : this.y - this.height / 2};
		}
		else if(this.rotation == 3 * Math.PI / 2){
			return {x : this.x - this.width / 2, y : this.y + this.height / 2};
		}
	}
		
	getMotor1(){
		if(this.rotation == Math.PI){
			return {x : this.x - this.width / 2, y : this.y + this.height / 2};
		}
		
		else if(this.rotation == 0){
			return {x : this.x + this.width / 2, y : this.y - this.height / 2};
		}
		else if(this.rotation == Math.PI / 2){
			return {x : this.x + this.width / 2, y : this.y + this.height / 2};
		}
		else if(this.rotation == 3 * Math.PI / 2){
			return {x : this.x - this.width / 2, y : this.y - this.height / 2};
		}	  
	}
	
	getMotor2(){
		if(this.rotation == Math.PI){
			return {x : this.x + this.width / 2, y : this.y - this.height / 2};
		}
		
		else if(this.rotation == 0){
			return {x : this.x - this.width / 2, y : this.y + this.height / 2};
		}
		else if(this.rotation == Math.PI / 2){
			return {x : this.x - this.width / 2, y : this.y - this.height / 2};
		}
		else if(this.rotation == 3 * Math.PI / 2){
			return {x : this.x + this.width / 2, y : this.y + this.height / 2};
		}	  
	}
	
	getMotor3(){
		if(this.rotation == Math.PI){
			return {x : this.x - this.width / 2, y : this.y - this.height / 2};
		}
		
		else if(this.rotation == 0){
			return {x : this.x + this.width / 2, y : this.y + this.height / 2};
		}
		else if(this.rotation == Math.PI / 2){
			return {x : this.x - this.width / 2, y : this.y + this.height / 2};
		}
		else if(this.rotation == 3 * Math.PI / 2){
			return {x : this.x + this.width / 2, y : this.y - this.height / 2};
		}	  
	}
	
	collide(meteor){
		var slope = Math.PI / 2 + calculateSlope(pShip, meteor);
		if(soundStatus) damageAudio.play();
		meteorCrashEffect(container.x + meteor.x, container.y + meteor.y, (180/Math.PI) * (Math.PI + slope), 1);
		
		this.x += pShip.speedX * Math.sin(slope);
		this.y -= pShip.speedY * Math.cos(slope);
		
		this.moveItems();
		
		this.energy -= meteor.damage;
		meteor.strength -= meteor.damage;	
	}
	
	collideDebris(debris){
	    var slope = Math.PI / 2 + calculateSlope(this, debris);
	    
	    this.x += pShip.speedX * Math.sin(slope);
		this.y -= pShip.speedY * Math.cos(slope);

		this.moveItems();
		
		this.energy -= debris.damage;
	}
	
	getBonusShield(){
		pShip.bonusShieldStatus = 1; 
		pShip.bonusShield.alpha = 1; 	
	}
	
	closeBonusShield(){
	    pShip.bonusShield.alpha = 0;
		pShip.bonusShieldStatus = 0; 
		clearTimeout(shieldItemTimeout);
	}
	
	doubleFactor(){
        pShip.updateFactor(pShip.factor * 2); 
		energyStep = (2 * step) / pShip.maxEnergy;	
	}
	
	halfFactor(){
	    pShip.updateFactor(pShip.factor / 2); 
        energyStep = (2 * step) / pShip.maxEnergy;
		clearTimeout(factorItemTimeout);
	}
		
    bulletHitAnim(bullet){
    	if (this.anim == null) {
			var frame = [];
			
			var anim = new PIXI.extras.AnimatedSprite(this.damageEffects);
			
			anim.x = bullet.x; 
			anim.y = bullet.y;
			anim.width = step / 2;
			anim.height = step / 2;
			anim.rotation = this.rotation + Math.PI / 2;
			anim.anchor.set(0.5);
			anim.alpha = 0.8;
			anim.animationSpeed = 0.05;
			this.anim = anim;
			anim.play();
			
			container.addChild(anim);
			
			this.animTimeout = setTimeout(function(){
				container.removeChild(anim);
				this.anim = null;
			}.bind(this),600);
		} else {
			clearTimeout(this.animTimeout);
			this.animTimeout = setTimeout(function(){
				container.removeChild(this.anim);
				this.anim = null;
			}.bind(this),600);
		}
    }
	
	controlStatus(){
		this.intervalId = setInterval(function(){
			this.readyLauncher = true;
			if(this.shieldStatus){
			    this.energy -= Math.floor(1 / this.factor);
			}
		}.bind(this),1000);
	}
	
}

class Enemy extends Ship{

	constructor(texture, energy = 100, factor = 1, item = null,
					bulletTextures = {}, deathItemsTextures = [], damageEffects = [], radar = null){
		super(texture, energy, factor);
		this.item = item;
		this.bulletTextures = bulletTextures;
		this.deathItemsTextures = deathItemsTextures;
		this.damageEffects = damageEffects;
		this.radar = radar;
		this.drop = false;
		this.readyLauncher = true;
		this.prevRotation = null;
		this.prevVX = null;
		this.prevVY = null;
		this.hitCount = 0;
		this.crashTime = 0;
		this.crashId = 0;
		this.timeoutId = 0;
		this.invisibilityStatus = 0;
		this.crashObject = {};
		this.teleportStatus = 0;
		this.alphaEffects = {};
		this.anim = null;
		this.state = "patrol";
		this.tempRotation = null;
		this.move = Enemy.defaultMove.bind(this);
		this.maxEnergy = this.energy * this.factor;
		this.transportTimeout = null;
		this.transportStatus = null;
		this.alive = true;
		
	}
	
	eFire(){
	    if(this.readyLauncher){
		    if(soundStatus) enemyFireAudio.play();
	        var eBullet = new energyBullet(this.bulletTextures["energyBullet"], this);
		    eBullet.width = step / 8;
		    eBullet.height = step / 2;
		    eBullet.x = this.x;
		    eBullet.y = this.y;
		    eBullet.initialPos.x = this.x;
		    eBullet.initialPos.y = this.y;
		    eBullet.range = 10 * step;
		    container.addChild(eBullet);
		    eBullets.push(eBullet);
		    this.readyLauncher = false;
		    this.timeoutId = setTimeout(function(){
		        this.readyLauncher = true;
		        clearTimeout(this.timeoutId);
		    }.bind(this), 1000);
		}
		
	}
	
	openRadar(){
		if(hitTestCircle(pShip, this.radar)){
			this.rotation = Math.PI/2 + calculateSlope(pShip, this.radar);
			this.state = "follow";
			this.move = Enemy.followMove.bind(this);
			this.eFire();
		}else if(!hitTestCircle(pShip, this.radar) && this.state == "follow"){
		    this.state = "patrol";
		    this.rotation = this.tempRotation;
		    this.move = Enemy.defaultMove.bind(this);
		}
		if(this.energy == 0){
			container.removeChild(this.radar);
			return false;
		}
				
		return true;
	}

    bulletHitAnim(bullet){
    	if (this.anim == null) {
			var frame = [];
			
			var anim = new PIXI.extras.AnimatedSprite(this.damageEffects);
			
			anim.x = bullet.x; 
			anim.y = bullet.y;
			anim.width = step / 2;
			anim.height = 2 * step / 2;
			anim.rotation = this.rotation + Math.PI / 2;
			anim.anchor.set(0.5);
			anim.alpha = 0.8;
			anim.animationSpeed = 0.05;
			this.anim = anim;
			anim.play();
			
			container.addChild(anim);
			
			this.animTimeout = setTimeout(function(){
				container.removeChild(anim);
				this.anim = null;
			}.bind(this),600);
		} else {
			clearTimeout(this.animTimeout);
			this.animTimeout = setTimeout(function(){
				container.removeChild(this.anim);
				this.anim = null;
			}.bind(this),600);
		}
    }
	
	static defaultMove(){
	    if(this.anim !== null){
	        this.anim.x = this.x;
	        this.anim.y = this.y;
	    }
	    
	    this.tempRotation = this.rotation;
	    
		if(contain(this, containerBounds) == undefined){
			this.x += this.speedX * Math.sin(this.rotation);
			this.y -= this.speedY * Math.cos(this.rotation);
			this.radar.x = this.x;
			this.radar.y = this.y; 
		}
		else{
			this.rotation = this.rotation + Math.PI;
			this.x += this.speedX * Math.sin(this.rotation);
			this.y -= this.speedY * Math.cos(this.rotation);
		    this.radar.x += this.x;
		    this.radar.y = this.y;		 
		}
	    
	}
	
	static followMove(){
	
	    if(this.anim !== null){
	        this.anim.x = this.x;
	        this.anim.y = this.y;
	    }
	    
		if(contain(this, containerBounds) == undefined){
			this.x += this.speedX * Math.sin(this.rotation);
			this.y -= this.speedY * Math.cos(this.rotation);
			this.radar.x = this.x;
			this.radar.y = this.y; 
		}else{
		    this.state == "patrol"
		    this.rotation = this.tempRotation;
		    this.move = Enemy.defaultMove.bind(this);
		}	
	}
	
	collide(meteor) {

		var slope = Math.PI / 2 + calculateSlope(this, meteor);
		
		particalEffect(container.x + this.x, container.y + this.y, (180/Math.PI) * (Math.PI + slope));
		meteorCrashEffect(container.x + meteor.x, container.y + meteor.y, (180/Math.PI) * (Math.PI + slope), 1);
		meteor.move = Meteor.pushedMoveMaker.bind(meteor)(step/10,Math.PI/2+slope,30);
		
		this.x += this.speedX * Math.sin(slope);
		this.y -= this.speedY * Math.cos(slope);
		
		this.radar.x += this.speedX * Math.sin(slope);
		this.radar.y -= this.speedY * Math.cos(slope);
		
		meteor.strength -= meteor.damage;
	   
	}
	
	collideDebris(){
	    this.teleport();
	}
	
	dealTeamCrash(){
		var curTime;
		curTime = new Date().getTime();
		eShips.forEach(function(eShip){
			if(calculateDistance(eShip, this) < 2 * step && eShip.id != this.id && (curTime-this.crashTime > 1000 || eShip.id != this.crashId) ){
				playerMotors(this.x + container.x, this.y + container.y, (180/Math.PI) * (this.rotation), 2);
				this.crashId = eShip.id;
				this.crashTime = curTime;
				this.rotation = (this.rotation + Math.PI) % (2*Math.PI);
				this.x += this.speedX * Math.sin(this.rotation);
				this.y -= this.speedY * Math.cos(this.rotation);
				if(!this.crashObject[eShip.id]){
				    this.crashObject[eShip.id] = 1;
				} else {
				    this.crashObject[eShip.id]++;
				}
			}else if(calculateDistance(eShip, this) < 2 * step && eShip.id != this.id && (curTime-this.crashTime <= 1000 || eShip.id == this.crashId) && this.crashObject[eShip.id] > 3){
			    this.crashObject[eShip.id] = 0;
			    this.crashTime = 0;
			    this.teleport();
			}else if(hitTestRectangle(eShip, this) && eShip.id != this.id && !eShip.teleportStatus && !this.teleportStatus && !eShip.transportStatus && !this.transportStatus){
			    this.energy = 0;
			    eShip.energy = 0;
			}
		}.bind(this));
	}
	
	teleport(){
	    if(soundStatus) enemyTransportAudio.play();
	    this.teleportStatus = 1;
	    var teleportPoint = this.calculateTeleportPoint();
	    var delCallback = function delCallback() {
	    	this.x = teleportPoint.tx;
			this.y = teleportPoint.ty;
			this.show(5);
			this.radar.x = this.x;
			this.radar.y = this.y;
			setTimeout(function(){this.teleportStatus = 0;}.bind(this), 2000);
	    }.bind(this);
	    this.del(5,delCallback);
	}
	
	calculateTeleportPoint(){
	    var teleportPoint = {'tx' : this.x - step, 'ty' : this.y + step};
	    for(var i = 0; i < 10; i++ ){
	        var newX = randomInt(-x + step, 2 * x - step);
	        var newY = randomInt(-y + step, 2 * y - step);
	        if(calculateDistance({'x' : newX, 'y' : newY}, this) >= 2 * step){
	            teleportPoint["tx"] = newX;
	            teleportPoint["ty"] = newY;
	            break;
	        }
	    }
	    
	    return teleportPoint;
	}
	
    del(count,callback){
        this.del1(count,count,callback);
    }
    
    del1(count,maxCount,callback){
	    if(count < 1){
	    	this.addAlphaEffect("Teleport",count/maxCount);
            return callback();
        }else{
        	this.addAlphaEffect("Teleport",count/maxCount);
            setTimeout(function(){
                this.del1(--count,maxCount,callback);
            }.bind(this), 200);
        }  
    }
        
    show(count){
        this.show1(count,count);
    }
    
    show1(count,maxCount){
	    if(count < 1){
	    	this.removeAlphaEffect("Teleport");
            return;
        }else{
        	this.addAlphaEffect("Teleport",1-count/maxCount);
            setTimeout(function(){
                this.show1(--count,maxCount);
            }.bind(this), 200);
        }
    }
    
	
	hit(bullet) {

	    if(this.energy > bullet.effect){
		    this.bulletHitAnim(bullet);
		}
		if(soundStatus) damageAudio.play();
		this.energy -= bullet.effect;
		
		var tempRotation = bullet.rotation % (2*Math.PI);
		if (this.hitCount == 0) {
			this.prevRotation = this.rotation;
			this.prevVX = this.speedX;
			this.prevVY = this.speedY;
			this.speedX = 0;
			this.speedY = 0;
		}
		this.hitCount++;
		if (tempRotation < 0) {
			tempRotation += 2*Math.PI;
		}
		if (tempRotation > Math.PI/4 && tempRotation <= 3*(Math.PI/4)) {
			this.rotation = 3*(Math.PI/2);
		} else if (tempRotation > 3*(Math.PI/4) && tempRotation <= 5*(Math.PI/4)) {
			this.rotation = 0;
		} else if (tempRotation > 5*(Math.PI/4) && tempRotation <= 7*(Math.PI/4)) {
			this.rotation = Math.PI/2;
		} else {
			this.rotation = Math.PI;
		}
		this.eFire();
		setTimeout(function(){
			this.rotation = this.prevRotation;
			this.speedX = this.prevVX;
			this.speedY = this.prevVY;
			
			this.hitCount--;
			if (this.hitCount == 0) {
				this.prevRotation = null;
				this.prevVX = null;
				this.prevVY = null;
			}
		}.bind(this), 200);
	}
    
    createOnNewLoc(newX, newY){
        this.x = newX;
        this.y = newY;
    }
	
	death(){
	    
	    if(soundStatus) deathAudio.play();
	    bulletCrashEffect(container.x + this.x, container.y + this.y, (180/Math.PI) * (this.rotation), 2, 1);
		
		var debris = new Debris(this.x, this.y, this.deathItemsTextures, 1);
		debris.id = this.id;
		debris.createDebris();
		debris.calculateRotParams();
		debris.s = (Math.random() > 0.5 ? -1 : 1) * Math.PI/3600;
		debrisList.push(debris);
		
		if(this.drop) {
		   	this.dropItem();
		}
		
		this.alive = false;
	    this.radar.x = -4 * x;
	    this.radar.y = -4 * y;		
		container.removeChild(this.radar);
	}
	
	dropItem(){
	    this.item.x = this.x;
	    this.item.y = this.y;
	    this.item.width = step / 2;
	    this.item.height = step / 2;
	    container.addChild(this.item);
	    this.item.dropTime = new Date().getTime();
	    this.item.dropStatus = true;
	
	}
	
	controlStatus(){
		this.intervalId = setInterval(function(){
			this.readyLauncher = true;
		}.bind(this),1000);
	}
	
	addAlphaEffect(name,value){
		this.alphaEffects[name] = value;
		this.alpha = Object.values(this.alphaEffects).reduce((finalAlpha,alphaEffect) => finalAlpha * alphaEffect,1);
		this.radar.alpha = this.alpha * 0.3;
	}
	
	removeAlphaEffect(name) {
		this.alphaEffects[name] = 1;
		this.alpha = Object.values(this.alphaEffects).reduce((finalAlpha,alphaEffect) => finalAlpha * alphaEffect,1);
		this.radar.alpha = this.alpha * 0.3;
	}
}

class Radar extends PIXI.Sprite{
	
	constructor(texture, obj, rotation){
		super(texture);
		this.obj = obj;
		this.anchor.set(0.5);
		this.rotation = rotation;
	}
}

class Shield extends PIXI.Sprite{
	
	constructor(texture, obj, rotation){
		super(texture);
		this.obj = obj;
		this.anchor.set(0.5);
		this.rotation = rotation;
	}
}

class Meteor extends PIXI.Sprite{

	constructor(texture){
		super(texture);
		this.anchor.set(0.5);
		this.strength = 100;
		this.damage = 10;
		this.t = 0;
		this.a = 0;
		this.b = 0;
		this.s = 0;
		this.state = "free";
		this.transporter = null;
		this.moveFrames = 0;
		this.groupId = null;
		this.status = "live";
		
		// functions
		this.move = Meteor.defaultMove.bind(this);
	}
	
	updateIt(){
	    this.damage = Math.floor(this.strength / 10);
	    this.width = Math.floor(step * (this.strength / 100));
	    this.height = Math.floor(step * (this.strength / 100));
	}
	
	calculateRotParams() {
		this.t = Math.atan2(this.y - rotationParams.o.y, this.x - rotationParams.o.x) + rotationParams.e;
		
		var m_t = {
			x : (this.x-rotationParams.o.x)*rotationParams.cos_e + (this.y-rotationParams.o.y)*rotationParams.sin_e,
			y : (this.y-rotationParams.o.y)*rotationParams.cos_e + (this.x-rotationParams.o.x)*rotationParams.sin_e
		};
		
		var c_2 = (((rotationParams.f0_t_x - rotationParams.f1_t_x) ** 2) / 4);
		var temp = (m_t.x ** 2) + (m_t.y ** 2) - c_2;
		
		var b_2 = ( temp + Math.sqrt( temp ** 2 + 4*c_2*(m_t.y ** 2)) ) / 2;
		this.b = Math.sqrt(b_2);
		this.a = Math.sqrt(b_2 + c_2);
	}
	
	static defaultMove() {
		this.t = (this.t + this.s) % (2*Math.PI);
		
		var m_t = {
			x : this.a * Math.cos(this.t),
			y : this.b * Math.sin(this.t)
		};
		
		this.x = m_t.x*rotationParams.cos_e + m_t.y*rotationParams.sin_e + rotationParams.o.x;
		this.y = m_t.y*rotationParams.cos_e - m_t.x*rotationParams.sin_e + rotationParams.o.y;
	}
	
	static pushedMoveMaker(speed, direction, frames) {
		this.moveFrames = frames;
		if (speed.length == frames) {
			return function(){
				this.x += speed[frames - this.moveFrames] * Math.cos(direction);
				this.y += speed[frames - this.moveFrames] * Math.sin(direction);
				this.moveFrames--;
				if (this.moveFrames <= 0) {
					this.calculateRotParams();
					this.move = Meteor.defaultMove.bind(this);
				}
			};
		} else {
			return function(){
				this.x += speed * Math.cos(direction);
				this.y += speed * Math.sin(direction);
				this.moveFrames--;
				if (this.moveFrames <= 0) {
					this.calculateRotParams();
					this.move = Meteor.defaultMove.bind(this);
				}
			};
		}
	}
	death(){
		meteorCrashEffect(container.x + this.x, container.y + this.y, (180/Math.PI) * (Math.PI * 2), 2);
		container.removeChild(this);
		this.x = -3 * x;
		this.y = -3 * y;
		this.status = "death";
	}
	
	resolveSelfCrash(){
		meteors.forEach(function(meteor){
			if(hitTestRectangle(this, meteor) && this.id != meteor.id){
		        var slope = calculateSlope(this, meteor);
	
				meteorCrashEffect(container.x + this.x, container.y + this.y, (180/Math.PI) * (Math.PI + slope), 1);
				meteorCrashEffect(container.x + meteor.x, container.y + meteor.y, (180/Math.PI) * (2 * Math.PI + slope), 1);
				this.x += step * Math.sin(slope);
				this.y -= step * Math.cos(slope);

				meteor.x += step * Math.sin(slope);
				meteor.y -= step * Math.cos(slope);

				this.strength -= meteor.damage;
				meteor.strength -= this.damage;
				    
				this.updateIt();  
				meteor.updateIt();
				    
            }
				
		}.bind(this));  
	}
}


