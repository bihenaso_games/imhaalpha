function particalEffect(beginX, beginY, angle){
	
	var emitter = new Proton.Emitter();
	
	emitter.rate = new Proton.Rate(10);
	
	emitter.addInitialize(new Proton.Body(path + 'spaceEffects_006.png'));
	//emitter.addInitialize(new Proton.Radius(1, 12));
	emitter.addInitialize(new Proton.Life(1));
	emitter.addInitialize(new Proton.Velocity(3, Proton.getSpan(angle, angle+20), 'polar'));

	//emitter.addBehaviour(new Proton.Color('FFFFFF', 'random'));
	emitter.addBehaviour(new Proton.Alpha(1, 0));
	emitter.p.x = beginX;
	emitter.p.y = beginY;
	emitter.emit('once');

	proton.addEmitter(emitter);

}

function meteorCrashEffect(beginX, beginY, angle, factor){
	
	var emitter = new Proton.Emitter();
	
	emitter.rate = new Proton.Rate(10 * factor);
	
	emitter.addInitialize(new Proton.Body(path + 'meteorBrown0.png'));
	emitter.addInitialize(new Proton.Life(1));
	emitter.addInitialize(new Proton.Velocity(3, Proton.getSpan(angle, angle + 180 * factor), 'polar'));

	emitter.addBehaviour(new Proton.Alpha(1, 0.5));
	emitter.addBehaviour(new Proton.Scale(.05, .05));
	emitter.addBehaviour(new Proton.Rotate(Proton.getSpan(0, 360), new Proton.Span([-1, -2, 0, 1, 2]), 'add'));
	emitter.p.x = beginX;
	emitter.p.y = beginY;
	emitter.emit('once');

	proton.addEmitter(emitter);

}

function playerMotors(beginX, beginY, angle, factor = 1){
	
	var emitter = new Proton.Emitter();
	
	emitter.rate = new Proton.Rate(10 * factor);
	
	emitter.addInitialize(new Proton.Body(path + 'spaceEffects_006.png'));
	//emitter.addInitialize(new Proton.Radius(1, 12));
	emitter.addInitialize(new Proton.Life(0.5));
	//emitter.addInitialize(new Proton.Velocity(3, Proton.getSpan(300, 320), 'polar'));

	emitter.addInitialize(new Proton.Position(new Proton.LineZone(beginX, beginY, beginX, beginY)));
	emitter.addInitialize(new Proton.Velocity(new Proton.Span(1, 3), angle, 'polar'));

	
	emitter.addBehaviour(new Proton.Alpha(0.5, 0));
	emitter.addBehaviour(new Proton.Scale(Proton.getSpan(.1, .3), .7));
	
	emitter.emit('once');

	proton.addEmitter(emitter);

}

function bulletCrashEffect(beginX, beginY, angle, factor, type){

	var emitter = new Proton.Emitter();
	console.log(angle);
	
	emitter.rate = new Proton.Rate(10 * factor);
	
	if(type == 0){
	    emitter.addInitialize(new Proton.Body(path + 'laserRed08.png'));
	}else if(type == 1){
	    emitter.addInitialize(new Proton.Body(path + 'laserBlue08.png'));
	}
	
	emitter.addInitialize(new Proton.Life(1 * factor));
	emitter.addInitialize(new Proton.Position(new Proton.LineZone(beginX - xStep / 2, beginY - yStep / 2, beginX + xStep / 2, beginY + yStep / 2)));
	emitter.addInitialize(new Proton.Velocity(new Proton.Span(1, 3), angle + 180, 'polar'));

	
	emitter.addBehaviour(new Proton.Alpha(0.8, 0.3));
	emitter.addBehaviour(new Proton.Scale(Proton.getSpan(.3, .5), .7));
	emitter.emit('once');

	proton.addEmitter(emitter);
}
