function calculateRotationParams() {
	rotationParams.o = {
		x : (rotationParams.f0.x + rotationParams.f1.x) / 2,
		y : (rotationParams.f0.y + rotationParams.f1.y) / 2
	};
	
	rotationParams.e = Math.atan2(rotationParams.f0.y - rotationParams.f1.y, rotationParams.f0.x - rotationParams.f1.x);
	
	rotationParams.sin_e = Math.sin(rotationParams.e);
	rotationParams.cos_e = Math.cos(rotationParams.e);
	
	rotationParams.f0_t_x = (rotationParams.f0.x - rotationParams.o.x)*rotationParams.cos_e + (rotationParams.f0.y - rotationParams.o.y)*rotationParams.sin_e;
	rotationParams.f1_t_x = (rotationParams.f1.x - rotationParams.o.x)*rotationParams.cos_e + (rotationParams.f1.y - rotationParams.o.y)*rotationParams.sin_e;
}

function contain(sprite, container){
	
	var collision = undefined;

  	if (sprite.x - sprite.width / 2 < container.x) {
		sprite.x = container.x + sprite.width;
		collision = "left";
  	}

  	if (sprite.y - sprite.height / 2  < container.y) {
		sprite.y = container.y + sprite.height;
		collision = "top";
  	}

  	if (sprite.x + sprite.width / 2 > container.width + container.x) {
		sprite.x = (container.width + container.x) - sprite.width;
		collision = "right";
  	}

  	if (sprite.y + sprite.height / 2 > container.height + container.y) {
		sprite.y = (container.height + container.y) - sprite.height;
		collision = "bottom";
  	}

  	return collision;
}

function contain1(sprite, container){
	
	var collision = undefined;

  	if (sprite.x <= container.x) {
		collision = "left";
  	}

  	if (sprite.y <= container.y) {
		collision = "top";
  	}

  	if (sprite.x >= container.width) {
		collision = "right";
  	}

  	if (sprite.y >= container.height) {
		collision = "bottom";
  	}

  	return collision;
}

function hitTestRectangle(r1, r2) {
	
	var hit, combinedHalfWidths, combinedHalfHeights, vx, vy;
	
	hit = false;
	
  	r1.centerX = r1.x;
  	r1.centerY = r1.y;
  	r2.centerX = r2.x;
  	r2.centerY = r2.y;

  	r1.halfWidth = r1.width / 2;
  	r1.halfHeight = r1.height / 2;
  	r2.halfWidth = r2.width / 2;
  	r2.halfHeight = r2.height / 2;

  	vx = r1.centerX - r2.centerX;
  	vy = r1.centerY - r2.centerY;

  	combinedHalfWidths = r1.halfWidth + r2.halfWidth;
  	combinedHalfHeights = r1.halfHeight + r2.halfHeight;

  	if (Math.abs(vx) < combinedHalfWidths){

		if (Math.abs(vy) < combinedHalfHeights){

	  		hit = true;
		}else{

	  		hit = false;	
		}
  	}else{

		hit = false;
  	}

  	return hit;
}

function hitTestCircle(c0,c1) {
	var combinedHalfWidths = c0.width/2 + c1.width/2;
	
	if (calculateDistance(c0,c1) <= combinedHalfWidths) {
		return true;
	}
	return false;
}

function calculateDistance(obj0, obj1){

	var distance, dx, dy, spod;
	
	dx = obj0.x - obj1.x;
	dy = obj0.y - obj1.y;
	
	spod = Math.pow(dx,2) + Math.pow(dy,2);
	distance = Math.pow(spod, 0.5);
	
	return Math.ceil(distance);
}

function calculateSlope(obj, obj1){

	return Math.atan2(obj.y - obj1.y, obj.x - obj1.x);
}

function randomInt(min, max){

	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function rotateElliptic(object, xCentre, yCentre, a, b, i){

	object.x = xCentre + (a * Math.cos(i));
	object.y = yCentre + (b * Math.sin(i));
		
}

function getAllUrlParams(url) {

	var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

	var obj = {};

	if (queryString) {

		queryString = queryString.split('#')[0];

		var arr = queryString.split('&');

		for (var i=0; i<arr.length; i++) {
			var a = arr[i].split('=');

			var paramNum = undefined;
			var paramName = a[0].replace(/\[\d*\]/, function(v) {
				paramNum = v.slice(1,-1);
				return '';
			});

			var paramValue = typeof(a[1])==='undefined' ? true : a[1];

			paramName = paramName.toLowerCase();
			paramValue = paramValue.toLowerCase();

			if (obj[paramName]) {
				if (typeof obj[paramName] === 'string') {
					obj[paramName] = [obj[paramName]];
				}
				if (typeof paramNum === 'undefined') {
					obj[paramName].push(paramValue);
				}
				else {
					obj[paramName][paramNum] = paramValue;
				}
			}
			else {
				obj[paramName] = paramValue;
			}
		}
	}

  return obj;
}
